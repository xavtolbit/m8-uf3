package cat.iam.m8.uf3.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import cat.iam.m8.uf3.Joc;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width=500;
		config.height=500;
		config.foregroundFPS = 10;
		new LwjglApplication(new Joc(), config);
	}
}
