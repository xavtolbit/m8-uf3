package cat.iam.m8.uf3.screens;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;

import cat.iam.m8.uf3.Constants;
import cat.iam.m8.uf3.Joc;

public class GameScreen extends ScreenAdapter {

	// Part grafica del videojoc
	Texture personatgeTex;
	Texture enemicTex;
	// Model de dades
	// Tant el jugador com els enemics son Rectangle
	Rectangle personatge;
	List<Rectangle> obstacles;
	float tempsAcumulat;
	
	final Joc joc;

	public GameScreen(Joc joc) {

		this.joc = joc;
		
		personatge = new Rectangle(Constants.PLAYER_POSX_INICIAL, Constants.PLAYER_POSY_INICIAL, Constants.PLAYER_AMPLE,
				Constants.PLAYER_ALT);

		personatgeTex = new Texture("grafics/player_blue.png"); // TODO cambiar a constant
		enemicTex = new Texture("grafics/enemy_red.png"); // TODO Cambiar a constant.

		// Inicialment no hi ha obstacles
		obstacles = new ArrayList<Rectangle>();
		tempsAcumulat = 0;
	}

	@Override
	public void render(float delta) {
		super.render(delta);
		// Sempre farem el seguent:
		// Input de l'usuari
		// Moure els elements de videojoc: enemics, dispars, colisions...
		// Dibuixar

		input(delta);
		update(delta);

		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		joc.batch.begin();
		joc.batch.draw(personatgeTex, personatge.x, personatge.y, personatge.width, personatge.height);
		for (Rectangle obstacle : obstacles) {
			joc.batch.draw(enemicTex, obstacle.x, obstacle.y, obstacle.width, obstacle.height);
		}
		joc.batch.end();
	}

	private void update(float delta) {

		// Preguntem a libgdx pel temps passat
		tempsAcumulat += delta;

		// Nou obstacle
		if (tempsAcumulat > Constants.TEMPS_NOU_ENEMIC) {
			tempsAcumulat = 0;// RESET. Important!!!

			// Generem aleatoriament la posicio dels obstacles
			// No agafem tot l'ample de pantalla. Deixem un poc de
			// marge per a que l'obstacle no estigui mig fora
			int amplePantalla = Gdx.graphics.getWidth();
			int posX = (int) ((amplePantalla - 30.0f) * Math.random());
			obstacles.add(new Rectangle(posX, 500, 30, 30));
		}

		// Moure obstacles
		for (Rectangle obstacle : obstacles) {
			obstacle.y = obstacle.y - Constants.ENEMIC_VELOCITAT_Y * delta;
		}

		// Eliminar obstacles
		for (int i = 0; i < obstacles.size(); i++) {
			Rectangle obstacle = obstacles.get(i);

			// Fora pantalla
			// Decidim eliminar els obstacles que toquen la part inferior
			if (obstacle.y < 0) {
				obstacles.remove(i);
			}

			// Colisions amb el player
			if (obstacle.overlaps(personatge)) {
				joc.setScreen(new GameoverScreen(joc));
//				Gdx.app.exit();
			}

		}

	}

	private void input(float delta) {

		// Preguntem a libgdx pel temps passat

		if (Gdx.input.isKeyPressed(Keys.RIGHT)) {
			personatge.x += Constants.VELOCITAT_PERSONATGE_X * delta;
		}
		if (Gdx.input.isKeyPressed(Keys.LEFT)) {
			personatge.x -= Constants.VELOCITAT_PERSONATGE_X * delta;
		}
		if (Gdx.input.isKeyPressed(Keys.UP)) {
			personatge.y += Constants.VELOCITAT_PERSONATGE_Y * delta;
		}
		if (Gdx.input.isKeyPressed(Keys.DOWN)) {
			personatge.y -= Constants.VELOCITAT_PERSONATGE_Y * delta;
		}

	}

	@Override
	public void dispose() {
		personatgeTex.dispose();
		enemicTex.dispose();
	}
}
