package cat.iam.m8.uf3.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Texture;

import cat.iam.m8.uf3.Constants;
import cat.iam.m8.uf3.Joc;

public class GameoverScreen extends ScreenAdapter{
	
	final Joc joc;
	Texture gameover;
	private float tempsAcumulat;
	
	public GameoverScreen(Joc joc) {
		this.joc = joc;
		gameover = new Texture("grafics/gameover.png");
	}
	
	@Override
	public void render(float delta) {
		super.render(delta);
		
		tempsAcumulat += delta;
		
		int width = Gdx.graphics.getWidth();
		int height = Gdx.graphics.getHeight();
		
		joc.batch.begin();
		joc.batch.draw(gameover, 0, 0, width, height);
		joc.batch.end();
		
		if(tempsAcumulat > Constants.TEMPS_GAMEOVER_SCREEN) 
		{
			this.dispose();
		}
	}
	
	@Override
	public void dispose() {
		super.dispose();
		gameover.dispose();
		Gdx.app.exit();
	}
}
