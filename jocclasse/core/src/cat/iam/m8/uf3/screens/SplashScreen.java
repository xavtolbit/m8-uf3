package cat.iam.m8.uf3.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Texture;

import cat.iam.m8.uf3.Constants;
import cat.iam.m8.uf3.Joc;

public class SplashScreen extends ScreenAdapter{

	final Joc joc;
	private Texture logo;
	
	float tempsAcumulat;
	
	public SplashScreen (Joc joc) {
		this.joc = joc;
		this.logo = new Texture("grafics/cdproject.png");
		tempsAcumulat = 0;
	}
	
	@Override
	public void render(float delta) {
		super.render(delta);
		
		tempsAcumulat += delta;
		
		if(tempsAcumulat > Constants.TEMPS_SPLASH_SCREEN) 
		{
			joc.setScreen(new GameScreen(joc));
			this.dispose();
		}
		else
		{
			int width = Gdx.graphics.getWidth();
			int height = Gdx.graphics.getHeight();
			
			joc.batch.begin();
			joc.batch.draw(logo, 0, 0, width, height);
			joc.batch.end();
		}
	}
	
	@Override
	public void dispose() {
		super.dispose();
		logo.dispose();
	}
}
