package cat.iam.m8.uf3;

public class Constants {

	public static final int PLAYER_AMPLE = 33;
	public static final int PLAYER_ALT = 33;
	public static final int PLAYER_POSX_INICIAL = 0;
	public static final int PLAYER_POSY_INICIAL = 0;
	public static final float TEMPS_NOU_ENEMIC = 1;
	public static final int ENEMIC_VELOCITAT_Y = 100;
	public static final int VELOCITAT_PERSONATGE_X = 100;
	public static final int VELOCITAT_PERSONATGE_Y = 100;
	public static final float TEMPS_SPLASH_SCREEN = 3;
	public static final float TEMPS_GAMEOVER_SCREEN = 5;
}
